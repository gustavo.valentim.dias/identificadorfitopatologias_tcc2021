/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { ColorSchemeName, Image, StyleSheet } from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import ModalScreen from '../screens/ModalScreen';
import NotFoundScreen from '../screens/NotFoundScreen';
import MainScreen from '../screens/MainScreen';
import HistoryScreen from '../screens/HistoryScreen';
import WeatherScreen from '../screens/WeatherScreen';
import CameraScreen from '../screens/CameraScreen';
import DiagnoseScreen from '../screens/DiagnoseScreen';
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import LinkingConfiguration from './LinkingConfiguration';
import { Text, View } from '../components/Themed';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen name="Modal" component={ModalScreen} />
      </Stack.Group>
      <Stack.Screen name="Câmera" component={CameraScreen} />
      <Stack.Screen name="Diagnóstico" component={DiagnoseScreen} />
      <Stack.Screen name="TelaPrincipal" component={MainScreen} />
      <Stack.Screen name="Histórico" component={HistoryScreen} />
    </Stack.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}>
      <BottomTab.Screen
        name="TabOne"
        component={MainScreen}
        options={({ }: RootTabScreenProps<'TabOne'>) => ({
          title: 'Tela principal',
          tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
          headerTitle: () => (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image style={styles.tinyLogo} source={require("../assets/images/logobar.png")}/>
              <Text style={{fontFamily: 'philosopher', color: '#002140', fontSize: 16}}> PlantID</Text>
            </View>
          ),
        })}
      />
      <BottomTab.Screen
        name="TabThree"
        component={WeatherScreen}
        options={{
          title: 'Clima',
          tabBarIcon: ({ color }) => <TabBarIcon name="sun-o" color={color} />,
          headerTitle: () => (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image style={styles.tinyLogo} source={require("../assets/images/logobar.png")}/>
              <Text style={{fontFamily: 'philosopher', color: '#002140', fontSize: 16}}> PlantID</Text>
            </View>
          ),
        }}
      />
      <BottomTab.Screen
        name="TabTwo"
        component={HistoryScreen}
        options={{
          title: 'Histórico',
          tabBarIcon: ({ color }) => <TabBarIcon name="book" color={color} />,
          headerTitle: () => (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image style={styles.tinyLogo} source={require("../assets/images/logobar.png")}/>
              <Text style={{fontFamily: 'philosopher', color: '#002140', fontSize: 16}}> PlantID</Text>
            </View>
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
  name: React.ComponentProps<typeof FontAwesome>['name'];
  color: string;
}) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}

const styles = StyleSheet.create({
  tinyLogo: {
    width: 45,
    height: 45,
    margin: 5,
  },

});