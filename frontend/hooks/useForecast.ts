import { useEffect, useState } from 'react';
import axios, { AxiosResponse } from 'axios';
import * as Location from 'expo-location'

const WEATHER_APP_KEY = '693a6d2f99f2be6d621d5b600c4e8b0e';

const useForecast = () => {

    useEffect(() => {
        Location.installWebGeolocationPolyfill();
        navigator.geolocation.getCurrentPosition((position) => {
            getWeather(position.coords.latitude, position.coords.longitude);
        })
    }, [])

    /*Pega dados API OpenWeather*/
    let getWeather = async (lat: number, long: number) => {
        let res = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${WEATHER_APP_KEY}`);
        // UpdateClimateInfo(res);
    }
    
    const UpdateClimateInfo = (info: any) => {
        fetch('http://127.0.0.1:8000/positioninginfo/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Accept': 'application/json, text/plain',
            },
            body: JSON.stringify({latitude:info.data['coord']['lat'], longitude:info.data['coord']['lon'], temperature: info.data['main']['temp'], 
            humidity:info.data['main']['humidity'], weather:info.data['weather']['0']['main'], city:info.data['name'], state:'São Paulo'})
        })
        .then(resp => resp.json())
        .then(data => {
            console.log(data)
        })
    }
}

export default useForecast;