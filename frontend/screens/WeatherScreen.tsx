import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import * as Location from 'expo-location'
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { weatherConditions } from '../utils/WeatherConditions';

export default function WeatherScreen({ navigation }: RootTabScreenProps<'TabOne'>) {
  const [temperature, setTemperature] = useState(null);
  const [weather, setWeather] = useState('Clear');
  const [country, setCountry] = useState(null);
  const [city, setCity] = useState(null);
  const [humidity, setHumidity] = useState(null);

/*Pega dados API OpenWeather*/
  let startWeatherInfo = async () => {
    const WEATHER_APP_KEY = '693a6d2f99f2be6d621d5b600c4e8b0e';

    useEffect(() => {
      Location.installWebGeolocationPolyfill();
      navigator.geolocation.getCurrentPosition((position) => {
          getWeather(position.coords.latitude, position.coords.longitude);
      })
    }, [])

  /*Pega dados API OpenWeather*/
    let getWeather = async (lat: number, long: number) => {
        let res = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${WEATHER_APP_KEY}&units=metric`);
        setCity(res.data.name);
        setCountry(res.data.sys.country);
        setWeather(res.data.weather[0].main);
        setTemperature(res.data.main.temp);
        setHumidity(res.data.main.humidity);
    }
  }

  startWeatherInfo();
  return (
    <View style={styles.weatherContainer}>
      <View style={styles.mainContainer}>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 30}}>
          <Text style={styles.tempText}>{city} - </Text>
          <Text style={styles.tempText}>{country}</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 30}}>
          <MaterialCommunityIcons size={48} name={weatherConditions[weather].icon} color={'#002140'} />
          <Text style={styles.title}> {weatherConditions[weather].title}</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 30}}>
          <MaterialCommunityIcons size={30} name='white-balance-sunny' color={'#002140'} />
          <Text style={styles.tempText}>  Temperatura: {temperature} °C</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 30}}>
          <MaterialCommunityIcons size={30} name='water' color={'#002140'} />
          <Text style={styles.tempText}> Umidade relativa do ar: {humidity}%</Text>
        </View>
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  weatherContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tempText: {
    fontFamily: 'philosopher',
    color: '#002140',
    fontSize: 20,
  },
  title: {
    fontSize: 25,
    fontFamily: 'philosopher',
    color: '#002140',
  },
});
