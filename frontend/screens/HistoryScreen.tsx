import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';

import { Text, View } from '../components/Themed';
import { History } from '../components/History';
import * as Requester from "../requester/data";
import axios from 'axios';
import { useEffect, useState } from 'react';

export default function HistoryScreen() {

  const [requestHistory, setRequestHistory] = useState(null);

  let getRequestHistory = async () => {
    const res = await axios.get('http://192.168.0.143:8000/reply/');
    setRequestHistory(res.data)
  }

  getRequestHistory()
  

  if (requestHistory){
    return (
      <View style={styles.container}>
        <ScrollView>
          <History historyData={requestHistory} />
        </ScrollView>
      </View>
    );
  }
  else{
    return null
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginBottom: 30,
    fontSize: 30,
    fontFamily: 'philosopher',
    color: '#002140',
    height: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
