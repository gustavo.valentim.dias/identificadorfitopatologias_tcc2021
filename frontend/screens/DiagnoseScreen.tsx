import * as React from 'react';
import { StyleSheet, Image, ActivityIndicator, ScrollView } from 'react-native';
import { Feather, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import { Text, View } from '../components/Themed';
import axios from 'axios';
import FormData from 'form-data'
import { useEffect, useState } from 'react';
import { images } from '../resources/diseaseImages';
import { useNavigation } from '@react-navigation/native';


export default function Diagnostico({ route }) {
  const {imageUri} = route.params;

  let filename = imageUri.split('/').pop();

  let match = /\.(\w+)$/.exec(filename);
  let type = match ? `image/${match[1]}` : `image`;

  let formData = new FormData();


  formData.append('photo', { uri: imageUri, name: filename, type });

  const [answer, setAnswer] = useState(null);
  const URL = 'http://192.168.0.143:8000/request/sendPhoto/';
  async function getDiagnosis(){
    await axios.post(URL, formData, {
      headers: {
       'content-type': 'multipart/form-data'
      }})
    let res = await axios.get(`http://192.168.0.143:8000/reply/predict`);
    
    if (res.data.diseaseFirst == "Saudável"){
      diseaseImage = 'saudavel'
    }
    else if (res.data.diseaseFirst == "Míldio"){
      diseaseImage = 'mildio'
    }
    else if (res.data.diseaseFirst == "Mancha foliar"){
      diseaseImage = 'mancha'
    }
    else if (res.data.diseaseFirst == "Mela"){
      diseaseImage = 'mela'
    }
    else if (res.data.diseaseFirst == "Podridão negra"){
      diseaseImage = 'podridao'
    }
    else{
      diseaseImage = 'ferrugem'
    }
    setAnswer(res.data)
  }


  useEffect(() => {
    var diseaseImage
    getDiagnosis()
  }, [])

  if (!answer){
    return (
      <View style={[styles.container]}>
        <Text style={{fontSize: 20, fontFamily: 'philosopher', color: '#002140', marginBottom: 5}}>
          Processando imagem</Text>
        <ActivityIndicator size="large" color="#00ff00" />
      </View>
    )
  }
  else{
    return (
      <View style={styles.mainContainer}>
        <ScrollView>
          <Text style={{alignItems: 'center', margin: 20, fontSize: 30, fontFamily: 'philosopher',
          color: '#002140'}}>{answer.diseaseFirst}</Text>
          <View style={styles.navBar}>
            <Image style={{ width: 175, height: 175, borderRadius: 50, margin: 20}} source={images[diseaseImage]}/>
          </View>
          <View style={styles.body}>
            <View style={styles.visaoGeral}>
              <Text style={{marginBottom:'5%', fontSize: 20, fontFamily: 'philosopher', color: '#002140'}}>Visão geral</Text>
              <Text style={{fontSize: 15, fontFamily: 'philosopher', color: '#002140'}}> 
              <MaterialCommunityIcons name="tree" size={20} color="#000"/> Sintomas</Text>
              <Text style={{margin: 20, fontSize: 12, fontFamily: 'philosopher', color: '#002140'}}>
                {answer.symptoms}
              </Text>
              <Text style={{fontSize: 15, fontFamily: 'philosopher', color: '#002140'}}>
                <FontAwesome name="flask" size={20} color="#000"/> Tratamento</Text>
              <Text style={{margin: 20, fontSize: 12, fontFamily: 'philosopher', color: '#002140'}}>
              {answer.treatment}
              </Text>
              <Text style={{fontSize: 15, fontFamily: 'philosopher', color: '#002140'}}
              ><FontAwesome name="heart" size={20} color="#000"/> Prevenção</Text>
              <Text style={{margin: 20, fontSize: 12, fontFamily: 'philosopher', color: '#002140'}}>
              {answer.prevention}
              </Text>
            </View>
            
          </View>
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  navBar: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
  },
  body: {
    flex: 3,
    display: 'flex',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  visaoGeral:{
    margin: 20,
  }
});
