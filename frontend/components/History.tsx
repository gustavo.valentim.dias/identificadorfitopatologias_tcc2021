import * as React from 'react';
import { ScrollView } from 'react-native';
import { Text, View } from './Themed';


export function History(props: any){
    var historyStructure;
    if (props.historyData.length == 0){
        historyStructure = <View><Text style={{fontSize: 15, fontFamily: 'philosopher', color: '#002140'}}>Você ainda não realizou pesquisas</Text></View>
    }
    else{
        historyStructure = props.historyData.map((history: any) => {
            return (
                <View style={{ padding: 10}} key={history.id} >
                    <View style={{ borderBottomColor: '#E5E5E5', borderBottomWidth: 1, width: 380,}}/>
                    <View style={{flexDirection: 'row', 
                                    alignItems: 'center',
                                    justifyContent: 'space-between'}}>
                        <Text style={{fontFamily: 'philosopher', color: '#002140', fontSize: 25, padding: 10}}>{history.diseaseFirst}</Text>
                        <Text style={{fontFamily: 'philosopher', color: '#002140', padding: 10}}>{new Date().toISOString().slice(0, 10)}</Text>
                    </View>
                        <Text style={{fontFamily: 'philosopher', fontSize: 10, color: '#002140', padding: 10}}>{history.additionalInformation}</Text>
                </View>
            );
        });
    
        historyStructure.push(
            <View style={{padding: 10}} key = {'Linha_final'}>
                    <View style={{ borderBottomColor: '#E5E5E5', borderBottomWidth: 1, width: 380}}/>
            </View>
        )
    }

    return historyStructure;
}