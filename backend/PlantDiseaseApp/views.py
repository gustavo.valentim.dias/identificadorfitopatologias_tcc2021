import os
from keras.layers.serialization import serialize
from numpy import asmatrix
import numpy as np
import os
import pickle
import tensorflow as tf
from keras.preprocessing.image import img_to_array
import matplotlib.pyplot as plt


from PlantDiseaseApp.PlantDiseaseClassifier import PrepareModelToPredict, PredictDisease
from PlantDiseaseApp.DiseaseInformation import setDiseaseInfoReply
from .serializers import UserSerializer, PositioningInfoSerializer, ReplySerializer, RequestSerializer
from .models import User, Request, PositioningInfo, Reply
from rest_framework.decorators import action
from rest_framework import viewsets
from .serializers import UserSerializer, PositioningInfoSerializer, ReplySerializer, RequestSerializer
from .models import User
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from django.core.files.storage import default_storage
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
import time

import json

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class RequestViewSet(viewsets.ModelViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer
    
    @action(detail=False, methods=['post'])
    def sendPhoto(self, request):


        # response = json.dumps([{}])
        file = request.data.get('photo')
        last_request = Request.objects.last()
        print(last_request)
        if last_request == None:
            filename = 'photo_1.jpg'
        else:
            index = last_request.id + 1
            filename = 'photo_' + str(index) + '.jpg'

        photo_path = 'posts/' + filename


        with default_storage.open(photo_path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        request = Request(image = photo_path)
        request.save()
        request_serializer = {'image': photo_path}
        reply = Reply(diseaseFirst = 'disease', treatment = 'treatment', prevention = 'prevention',
        symptoms = 'symptoms', additionalInformation = 'additionalInformation', probabilityDiseaseFirst = 50,
         probabilityDiseaseSecond = 20, diseaseSecond = 'disease')
        reply.save()

        return Response(request_serializer)

    @action(detail=False, methods=['post'])
    def sendImage(self, request):

        serializer = RequestSerializer(data=request.data)
        reply = Reply(diseaseFirst = 'disease', treatment = 'treatment', prevention = 'prevention',
        symptoms = 'symptoms', additionalInformation = 'additionalInformation', probabilityDiseaseFirst = 50,
         probabilityDiseaseSecond = 20, diseaseSecond = 'disease')
        reply.save()

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        requests = Request.objects.all()
        requests_serializer = RequestSerializer(requests, many = True)
        return Response(requests_serializer.data)

class PositioningInfoViewSet(viewsets.ModelViewSet):
    queryset = PositioningInfo.objects.all()
    serializer_class = PositioningInfoSerializer

class ReplyViewSet(viewsets.ViewSet):

    @action(detail=False, methods=['get'])
    def predict(self, request):


        lastRequest = Request.objects.last()
        
        lastReply = Reply.objects.last()
        requests_serializer = RequestSerializer(lastRequest)
        image = requests_serializer.data['image']

        model = PrepareModelToPredict()
        diseases = PredictDisease(os.path.abspath(os.getcwd()) + image, model)
        
        classes = ['blight', 'healthy', 'mildew', 'rot', 'rust', 'spot']

        indexDiseaseFirst = np.argmax(diseases)
        diseaseFirst = classes[indexDiseaseFirst]
        print(classes[np.argmax(diseases)])

        probabilityDiseaseFirst = str(diseases)
        probabilityDiseaseFirst = probabilityDiseaseFirst[2:-2]
        probabilityDiseaseFirst = probabilityDiseaseFirst.split()

        probabilityDiseaseFirst = list(probabilityDiseaseFirst)
        for i in range(len(probabilityDiseaseFirst)):
            probabilityDiseaseFirst[i] = '{:.20f}'.format(float(probabilityDiseaseFirst[i]))
        print(probabilityDiseaseFirst)

        lastReply.diseaseFirst = diseaseFirst
        lastReply.probabilityDiseaseFirst = max(probabilityDiseaseFirst)
        
        lastReply = setDiseaseInfoReply(lastReply)
        lastReply.image = image
        lastReply.save()

        serializer = {'id': lastReply.id, 'image': image, 'diseaseFirst': lastReply.diseaseFirst,
         'treatment': lastReply.treatment, 'prevention': lastReply.prevention, 'symptoms': lastReply.symptoms, 'additionalInformation': lastReply.additionalInformation, 'probabilityDiseaseFirst': max(probabilityDiseaseFirst),
          'probabilityDiseaseSecond': lastReply.probabilityDiseaseSecond, 'diseaseSecond': lastReply.diseaseSecond}

        return Response(serializer)
        
    def list(self, request):
        replys = Reply.objects.all()
        replys_serializer = ReplySerializer(replys, many = True)
        return Response(replys_serializer.data)