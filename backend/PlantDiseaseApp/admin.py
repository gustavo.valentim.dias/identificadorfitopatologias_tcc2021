from django.contrib import admin
from django.contrib.admin.filters import ListFilter
from .models import User, Request, PositioningInfo, Reply

# Register your models here.

admin.site.register(User)

admin.site.register(Request)

admin.site.register(PositioningInfo)

admin.site.register(Reply)