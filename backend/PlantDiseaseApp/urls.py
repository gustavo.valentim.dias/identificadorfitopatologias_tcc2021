"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include

from .views import UserViewSet, RequestViewSet, PositioningInfoViewSet, ReplyViewSet
from rest_framework.routers import DefaultRouter

from PlantDiseaseApp import views

router = DefaultRouter()
router.register('users', UserViewSet, basename='users')
router.register('request', RequestViewSet, basename='request')
router.register('positioninginfo', PositioningInfoViewSet, basename='positioninginfo')
router.register('reply', ReplyViewSet, basename='reply')

urlpatterns = [
    path('', include(router.urls)),
]
