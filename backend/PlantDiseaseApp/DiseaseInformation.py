from PlantDiseaseApp.constants.adittionalInformation import BLIGHT_ADDITIONALINFO, MILDEW_ADDITIONALINFO, ROT_ADDITIONALINFO, RUST_ADDITIONALINFO, SPOT_ADDITIONALINFO
from PlantDiseaseApp.constants.prevention import BLIGHT_PREVENTION, MILDEW_PREVENTION, ROT_PREVENTION, SPOT_PREVENTION
from PlantDiseaseApp.constants.symptoms import BLIGHT_SYMPTOMS, MILDEW_SYMPTOMS, ROT_SYMPTOMS, RUST_SYMPTOMS, SPOT_SYMPTOMS
from PlantDiseaseApp.constants.treatment import BLIGHT_TREATMENT, MILDEW_TREATMENT, ROT_TREATMENT, SPOT_TREATMENT

def setDiseaseInfoReply(lastReply):

    if lastReply.diseaseFirst == 'spot':

        lastReply.diseaseFirst = 'Mancha foliar'
        lastReply.symptoms = SPOT_SYMPTOMS
        lastReply.treatment = SPOT_TREATMENT
        lastReply.prevention = SPOT_PREVENTION
        lastReply.additionalInformation = SPOT_ADDITIONALINFO

    elif lastReply.diseaseFirst == 'rust':

        lastReply.diseaseFirst = 'Ferrugem '
        lastReply.symptoms = RUST_SYMPTOMS
        lastReply.treatment = ROT_TREATMENT
        lastReply.prevention = ROT_PREVENTION
        lastReply.additionalInformation = RUST_ADDITIONALINFO

    elif lastReply.diseaseFirst == 'blight':

        lastReply.diseaseFirst = 'Mela'
        lastReply.symptoms = BLIGHT_SYMPTOMS
        lastReply.treatment = BLIGHT_TREATMENT
        lastReply.prevention = BLIGHT_PREVENTION
        lastReply.additionalInformation = BLIGHT_ADDITIONALINFO

    elif lastReply.diseaseFirst == 'rot':

        lastReply.diseaseFirst = 'Podridão negra'
        lastReply.symptoms = ROT_SYMPTOMS
        lastReply.treatment = ROT_TREATMENT
        lastReply.prevention = ROT_PREVENTION
        lastReply.additionalInformation = ROT_ADDITIONALINFO

    elif lastReply.diseaseFirst == 'mildew':

        lastReply.diseaseFirst = 'Míldio'
        lastReply.symptoms = MILDEW_SYMPTOMS
        lastReply.treatment = MILDEW_TREATMENT
        lastReply.prevention = MILDEW_PREVENTION
        lastReply.additionalInformation = MILDEW_ADDITIONALINFO
        
    elif lastReply.diseaseFirst == 'healthy':

        lastReply.diseaseFirst = 'Saudável'
        lastReply.symptoms = 'A planta não apresenta nenhum sintoma, mantenha os cuidados feitos'
        lastReply.treatment = 'A planta está em perfeito estado, continue com os cuidados diários feitos'
        lastReply.prevention = 'Para prevenir doenças, consulte informativos sobre a espécie fotografada'
        lastReply.additionalInformation = 'A planta está em perfeito estado, continue com os cuidados diários feitos'

    return lastReply
